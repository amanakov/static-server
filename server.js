var StaticServer = require('static-server')

var server = new StaticServer({
  rootPath: './build',
  name: 'my-http-server',
  port: 8080,
  cors: '*',
  followSymlink: true
})

server.start(() => {
  console.log('Server listening to', server.port)
})
